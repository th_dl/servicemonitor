# TODO list

- Rename project import to gitlab.com/...
- Finish documentation part, break down each type of service
- Make status tables draggable ?
- Finish groups feature ...
- Possibility to download config file from web interface ?
- Fix xml export
- Move all static files in a new directory, reorganize them and embed it in an embed.FS to serve it entierly
- Regex check for checkport network ("ip4:1" ...)
