package main

import (
	"fmt"
	"net/http"
	"strings"

	"ServiceMonitor/src/static"
)

// Doc represents a handler for the documentation part
type Doc struct {
	path string
}

// DocFrom returns a new Doc object with the given root path
func DocFrom(root string) Doc {
	return Doc{path: root}
}

// SoloStart can be used to start the web server with only the documentation part
func (doc *Doc) SoloStart(port string) {
	if !strings.HasSuffix(doc.path, "/") {
		doc.path += "/"
	}
	static.InitDocPath(doc.path)
	http.Handle(doc.path, doc)
	http.HandleFunc("/static/", staticHandler)
	exitWithError(http.ListenAndServe(":"+port, nil))
}

func (doc Doc) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	docRoot := strings.ToLower(r.URL.Path)
	docRoot = strings.TrimPrefix(docRoot, doc.path)
	docRoot = strings.Split(docRoot, "/")[0]
	switch docRoot {
	case "":
		http.Redirect(w, r, doc.path+"index", http.StatusSeeOther)
	case "index":
		doc.index(w, r)
	case "server":
		doc.server(w, r)
	case "config":
		doc.config(w, r)
	case "ui":
		doc.ui(w, r)
	case "res":
		doc.resources(w, r)
	default:
		notFoundHandler(w, r)
	}
}

func (doc Doc) index(w http.ResponseWriter, r *http.Request) {
	static.PrintDocIndex(w)
	/*w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	template.Must(template.ParseFiles("/home/maxi/go/src/ServiceMonitor/resources/doc/index.tmpl")).Execute(w, struct {
		DocPath string
	}{DocPath: doc.path})*/
}

func (doc Doc) server(w http.ResponseWriter, r *http.Request) {
	static.PrintDocServer(w)
	/*w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	template.Must(template.ParseFiles("/home/maxi/go/src/ServiceMonitor/resources/doc/server.tmpl")).Execute(w, struct {
		DocPath string
	}{DocPath: doc.path})*/
}

func (doc Doc) config(w http.ResponseWriter, r *http.Request) {
	static.PrintDocConfig(w)
	/*w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	template.Must(template.ParseFiles("/home/maxi/go/src/ServiceMonitor/resources/doc/config.tmpl")).Execute(w, struct {
		DocPath string
	}{DocPath: doc.path})*/
}

func (doc Doc) ui(w http.ResponseWriter, r *http.Request) {
	static.PrintDocUI(w)
	/*w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	template.Must(template.ParseFiles("/home/maxi/go/src/ServiceMonitor/resources/doc/ui.tmpl")).Execute(w, struct {
		DocPath string
	}{DocPath: doc.path})*/
}

func (doc Doc) resources(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(strings.TrimPrefix(r.URL.Path, "/"), "/")
	if len(parts) < 2 {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintln(w, "Invalid resource file path")
		return
	}
	reqFile := parts[1]
	var fileBytes []byte
	var err error
	switch reqFile {
	case "prism.js":
		static.PrintPrismCss(w)
		// fileBytes, err = ioutil.ReadFile("/home/maxi/go/src/ServiceMonitor/resources/prism.js")
	case "prism.css":
		static.PrintPrismJs(w)
		// fileBytes, err = ioutil.ReadFile("/home/maxi/go/src/ServiceMonitor/resources/prism.css")
	default:
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "Resource %q not found\n", r.URL.Path)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintln(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(fileBytes)
}
