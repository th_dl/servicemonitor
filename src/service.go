package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

// Service represents a task to be executed at a precise interval
type Service struct {
	group            string
	name             string
	command          Shell
	httpRequest      Http
	checkPort        CheckPort
	period           time.Duration
	warningThreshold time.Duration
	errorThreshold   time.Duration
	confIsValid      bool
	execStatus       []ServiceStatus
	gauge            prometheus.Gauge
}

// Shell represents a shell command and its arguments
type Shell struct {
	active  bool
	CmdName string   `yaml:"command"`
	Args    []string `yaml:"args"`
}

// Http represents a GET request and http codes that will be considered successful
type Http struct {
	active           bool
	Url              string `yaml:"url"`
	HttpSuccessCodes []uint `yaml:"httpSuccessCodes"`
	client           http.Client
}

// CheckPort represents a connection establishment to its host on its port
type CheckPort struct {
	active   bool
	Host     string `yaml:"host"`
	Port     uint16 `yaml:"port"`
	Protocol string `yaml:"protocol"`
	strPort  string
}

// String returns the 'path' of the service (group and name separated by a slash)
func (serv Service) String() string {
	return serv.group + "/" + serv.name
}

// toString returns a detailed view of the service and its properties
func (serv Service) toString() string {
	var msg string
	padding := "           "
	if serv.command.active {
		msg = fmt.Sprintf("\n\tCommand:%s%v\n", padding, serv.command)
	}
	if serv.httpRequest.active {
		msg += fmt.Sprintf("\n\tHTTP request:      %s\n", serv.httpRequest.Url)
	}
	if serv.checkPort.active {
		msg += fmt.Sprintf("\n\tHost:   %s%s\n\tPort:   %s%d\n", padding, serv.checkPort.Host, padding, serv.checkPort.Port)
	}
	return fmt.Sprintf("%s\tPeriod: %s%v\n\tWarning threshold: %v\n\tError threshold:   %v", msg, padding, serv.period, serv.warningThreshold, serv.errorThreshold)
}

// exportProperties returns a ServProperties object that contains all the properties of the service
func (serv Service) exportProperties() ServProperties {
	props := ServProperties{Group: serv.group}
	if serv.command.active {
		props = ServProperties{
			Type:    "command",
			Command: serv.command.CmdName,
			Args:    strings.Join(serv.command.Args, " "),
		}
	} else if serv.httpRequest.active {
		props = ServProperties{
			Type:             "httpRequest",
			Url:              serv.httpRequest.Url,
			HttpSuccessCodes: serv.httpRequest.HttpSuccessCodes,
		}
	} else if serv.checkPort.active {
		props = ServProperties{
			Type:     "checkPort",
			Host:     serv.checkPort.Host,
			Port:     serv.checkPort.Port,
			Protocol: serv.checkPort.Protocol,
		}
	} else {
		return ServProperties{Type: "none"}
	}
	props.Period = serv.period.String()
	props.WarningThreshold = serv.warningThreshold.String()
	props.ErrorThreshold = serv.errorThreshold.String()
	props.LastStatus = serv.execStatus[len(serv.execStatus)-1].LastValue
	return props
}

// run starts the service's infinite cycle, at least as long as its configuration is valid
func (serv *Service) run() {
	servPath := serv.name
	if conf.useGroups {
		servPath = serv.group + "/" + servPath
	}
	log(servPath, "Starting service ...")
	go func() {
		for serv.confIsValid {
			startTime := time.Now()
			var status ServiceStatus
			if serv.command.active {
				status = serv.execCmd()
			} else if serv.httpRequest.active {
				status = serv.httpGet()
			} else {
				status = serv.checkport()
			}

			if status.LastValue == statusNoError {
				serv.gauge.Set(float64(status.ExecDuration))
			} else {
				log(servPath, status.LastValue)
				serv.gauge.Set(-1)
			}
			serv.execStatus = append(serv.execStatus, status)
			if len(serv.execStatus) > conf.maxDataCount {
				serv.execStatus = serv.execStatus[1:]
			}
			time.Sleep(serv.period - time.Since(startTime))
		}
	}()
}

// execCmd executes the defined bash command with the defined arguments, and returns a ServiceStatus
func (serv Service) execCmd() ServiceStatus {
	startTime := time.Now()
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, serv.errorThreshold)
	defer cancel()
	cmd := exec.CommandContext(ctx, serv.command.CmdName, serv.command.Args...)
	cmd.Stdin = os.Stdin
	err := cmd.Run()
	totalTime := time.Since(startTime)
	status := ServiceStatus{
		ExecTime:     startTime.Unix(),
		ExecDuration: totalTime.Milliseconds(),
	}
	if err != nil {
		if ctx.Err() != nil {
			status.Status = StatusTimeout
			status.LastValue = ctx.Err().Error()
		} else {
			status.Status = StatusError
			status.LastValue = err.Error()
		}
	} else {
		status.Status = serv.inferStatus(totalTime)
		status.LastValue = statusNoError
	}
	return status
}

// httpGet executes a GET request on the defined host and returns a ServiceStatus
func (serv Service) httpGet() ServiceStatus {
	startTime := time.Now()
	resp, err := serv.httpRequest.client.Get(serv.httpRequest.Url)
	totalTime := time.Since(startTime)
	status := ServiceStatus{
		ExecTime:     startTime.Unix(),
		ExecDuration: totalTime.Milliseconds(),
	}
	if err != nil {
		status.Status = StatusError
		status.LastValue = err.Error()
	} else {
		statusCodeIsValid := serv.httpRequest.isValidStatusCode(resp.StatusCode)
		if statusCodeIsValid {
			status.Status = serv.inferStatus(totalTime)
			status.LastValue = statusNoError
		} else {
			status.Status = StatusError
			status.LastValue = "Response status code not valid"
		}
	}
	return status

}

// checkPort tries to establish a connection to the defined host on the defined port, and returns a ServiceStatus
func (serv Service) checkport() ServiceStatus {
	startTime := time.Now()
	conn, err := net.DialTimeout(serv.checkPort.Protocol, net.JoinHostPort(serv.checkPort.Host, serv.checkPort.strPort), serv.errorThreshold)
	totalTime := time.Since(startTime)
	status := ServiceStatus{
		ExecTime:     startTime.Unix(),
		ExecDuration: totalTime.Milliseconds(),
	}
	if err != nil {
		status.Status = StatusError
		status.LastValue = err.Error()
	} else if conn == nil {
		status.Status = StatusError
		status.LastValue = "Failed to open the connection"
	} else {
		closeErr := conn.Close()
		if closeErr != nil {
			status.Status = StatusError
			status.LastValue = "Failed to close the connection"
		} else {
			status.Status = serv.inferStatus(totalTime)
			status.LastValue = statusNoError
		}
	}
	return status
}
