package main

import (
	"fmt"
	"regexp"
	"strings"
	"time"
)

const (
	StatusOk      = "OK"
	StatusWarn    = "WARN"
	StatusError   = "ERROR"
	StatusTimeout = "TIMEOUT"

	statusNoError = "no error"
)

// ServiceStatus represents the status of a service execution
type ServiceStatus struct {
	// ExecTime is the time of the execution of the action, stored as the number of seconds elapsed since January 1, 1970
	ExecTime     int64  `json:"exec_time" yaml:"execTime" xml:"execTime"`
	ExecDuration int64  `json:"exec_duration" yaml:"execDuration" xml:"execDuration"`
	Status       string `json:"status" yaml:"status" xml:"status"`
	LastValue    string `json:"last_value" yaml:"lastValue" xml:"lastValue"`
}

// getDuration tries to parse the given prop as a duration, returns it or crashes if the parsing fails
func getDuration(group, prop, object string) time.Duration {
	var groupPrecision string
	if conf.useGroups && len(group) != 0 {
		groupPrecision = " of group '" + group + "'"
	}
	t, err := time.ParseDuration(prop)
	if err != nil {
		exitWithError(fmt.Errorf("error while parsing %s%s: %v", object, groupPrecision, err))
	}
	if t <= 0 {
		exitWithError(fmt.Errorf("error while parsing %s%s: value must be positive", object, groupPrecision))
	}
	return t
}

// inferStatus returns the string representation of the service's status
func (serv Service) inferStatus(execTime time.Duration) string {
	if execTime < 0 {
		return StatusError
	}
	if execTime < serv.warningThreshold {
		return StatusOk
	}
	if execTime < serv.errorThreshold {
		return StatusWarn
	}
	return StatusTimeout
}

// makeStatusMapFrom gathers the detailed status of every service of every group into a marshallable map
func (services Services) makeStatusMapFrom() map[string]map[string]ServiceStatus {
	servMap := make(map[string]map[string]ServiceStatus)
	for groupName, serviceGroup := range services {
		groupMap := map[string]ServiceStatus{}
		for servName, serv := range serviceGroup {
			if len(serv.execStatus) == 0 {
				continue
			}
			groupMap[servName] = serv.execStatus[len(serv.execStatus)-1]
		}
		servMap[groupName] = groupMap
	}
	return servMap
}

// makeHistoryMapFrom gathers all the detailed status from the start of the application, of every service of every group
func (services Services) makeHistoryMapFrom() map[string]map[string][]ServiceStatus {
	historyMap := make(map[string]map[string][]ServiceStatus)
	for groupName, serviceGroup := range services {
		groupMap := map[string][]ServiceStatus{}
		for servName, serv := range serviceGroup {
			/*var statusList []ServiceStatus
			for _, execTime := range serv.execStatus {
				statusList = append(statusList, ServiceStatus{
					ExecDuration: execTime.Milliseconds(),
					Status:       serv.getStatusFromExecTime(execTime),
				})
			}*/
			groupMap[servName] = serv.execStatus
		}
		historyMap[groupName] = groupMap
	}
	return historyMap
}

// normalizeNameForPrometheus replaces by an underscore every character in the given name that isn't allowed
// in a prometheus service or group name
func normalizeNameForPrometheus(name string) string {
	name = strings.ToLower(name)
	re := regexp.MustCompile(`[^a-zA-Z0-9]`)
	return re.ReplaceAllString(name, "_")
}
