package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"ServiceMonitor/src/static"
)

func notFoundHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusNotFound)
	tmplt, err := static.Get404Template()
	if err != nil {
		log("[HTTP-Server]", "Failed to load 404 template:", err)
		fmt.Fprintln(w, "Page not found + Internal error")
		return
	}
	tmplt.Execute(w, struct {
		PageNotFound string
	}{
		PageNotFound: r.URL.Path,
	})
}

func exportHandler(successMap map[string]map[string]ServiceStatus, w http.ResponseWriter, r *http.Request) {
	data := map[string]map[string]ServiceStatus{}
	_ = r.ParseForm()
	services, ok := r.Form["service"]
	if !ok || len(services) < 1 {
		data = successMap
	} else {
		for _, service := range services {
			status, exists := successMap[service]
			if exists {
				data[service] = status
			}
		}
	}
	format := r.FormValue("format")
	if format == "raw" {
		w.Header().Set("Content-Type", "text/plain")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusOK)
		for serv, status := range successMap {
			fmt.Fprintln(w, serv+":", status)
		}
	} else {
		encodeAndWrite(w, format, data)
	}
}

func exportHistoryHandler(historyMap map[string]map[string][]ServiceStatus, interval float64, w http.ResponseWriter, r *http.Request) {
	data := struct {
		Interval   float64                               `json:"interval" yaml:"interval"`
		LastUpdate time.Time                             `json:"last_update" yaml:"last_update"`
		History    map[string]map[string][]ServiceStatus `json:"history" yaml:"history"`
	}{
		Interval:   interval,
		LastUpdate: time.Now(),
		History:    historyMap,
	}
	_ = r.ParseForm()
	services, ok := r.Form["service"]
	if ok || len(services) > 0 {
		history := map[string]map[string][]ServiceStatus{}
		for _, service := range services {
			status, exists := historyMap[service]
			if exists {
				history[service] = status
			}
		}
		data.History = history // or delete() unwanted services ?
	}
	lastWantedValues, err := strconv.Atoi(strings.ToLower(r.FormValue("dataCount")))
	if err == nil && lastWantedValues > 0 && lastWantedValues < 100 {
		for groupName, serviceGroup := range data.History {
			for service, statusList := range serviceGroup {
				if len(statusList) > lastWantedValues {
					data.History[groupName][service] = statusList[len(statusList)-lastWantedValues:]
				}
			}
		}
	}
	w.Header().Set("Access-Control-Allow-Origin", "*")
	encodeAndWrite(w, r.FormValue("format"), data)
}

func exportPropertiesHandler(services *Services, w http.ResponseWriter, r *http.Request) {
	_ = r.ParseForm()
	servGroup := r.FormValue("group")
	servName := r.FormValue("service")
	if len(servGroup) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "No group provided")
		return
	}
	if len(servName) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "No service provided")
		return
	}
	format := r.FormValue("format")
	if len(format) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "No format provided")
		return
	}
	service, exist := services.getService(servGroup, servName)
	if !exist {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Service %q of group %q not found\n", servName, servGroup)
		return
	}
	w.Header().Set("Access-Control-Allow-Origin", "*")
	encodeAndWrite(w, format, service.exportProperties())
}

func historyHandler(historyMap map[string]map[string][]ServiceStatus, interval string, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	jsonMap, err := json.Marshal(historyMap)
	if err != nil {
		log("[HTTP-Server]", "Failed to marshal history map")
		return
	}
	tmplt, err := static.GetHistoryTemplate()
	if err != nil {
		log("[HTTP-Server]", "Failed to load history template:", err)
		fmt.Fprintln(w, "Internal error")
		return
	}
	hostname, _ := os.Hostname()
	err = tmplt.Execute(w, static.HistoryFill{
		HistoryMap: template.JS(jsonMap),
		Interval:   template.JS(interval),
		Hostname:   hostname,
		Version:    version,
	})
	if err != nil {
		log("[HTTP-Server]", "Failed to execute history template:", err)
	}
}

func reloadHandler(confPath string, services *Services, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintln(w, "Only POST method is allowed for reload request")
		return
	}
	services.unsetGauges()
	err := services.reloadConfig(loadYaml(confPath))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintln(w, "Error:", err)
		exitWithError(err)
	}
	services.startAll(conf.httpRefreshPeriod, new(sync.WaitGroup))
	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, "Config reloaded !")
}

func staticHandler(w http.ResponseWriter, r *http.Request) {
	pathEnd := strings.Count(r.URL.Path, "/")
	file := strings.Split(r.URL.Path, "/")[pathEnd]
	switch file {
	case "global.css":
		static.PrintGlobalCss(w)
	case "prism.js":
		static.PrintPrismJs(w)
	case "prism.css":
		static.PrintPrismCss(w)
	case "doc.css":
		static.PrintDocCss(w)
	case "DejaVuSansMono.ttf":
		static.PrintDejaVuSansMono(w)
	default:
		w.WriteHeader(http.StatusNotFound)
	}
}
