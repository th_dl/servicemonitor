package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var hostname, _ = os.Hostname()

// Services represents all the registered services
type Services map[string]ServiceGroup

// ServiceGroup represents a group of services
type ServiceGroup map[string]*Service

// YamlStruct represents the structure of the yaml configuration file.
// A pointer to a YamlStruct is given to the Yaml parser to unmarshal the config.
type YamlStruct struct {
	Services map[string]map[string]struct {
		Command          Shell     `yaml:"shell"`
		Request          Http      `yaml:"http"`
		CheckPort        CheckPort `yaml:"checkPort"`
		Period           string    `yaml:"period"`
		WarningThreshold string    `yaml:"warningThreshold"`
		ErrorThreshold   string    `yaml:"errorThreshold"`
	} `yaml:"services"`
	Config struct {
		HttpRefreshPeriod string `yaml:"httpRefreshPeriod"`
		MaxDataCount      uint   `yaml:"maxDataCount"`
	} `yaml:"config"`
}

// ServProperties represents all the properties a service can have,
// combining all the types of service. It is used to export the properties
// of a service to the web interface
type ServProperties struct {
	Group            string `yaml:"group" json:"group"`
	Type             string `yaml:"type" json:"type"`
	Command          string `yaml:"command" json:"command"`
	Args             string `yaml:"arguments" json:"arguments"`
	Url              string `yaml:"url" json:"url"`
	HttpSuccessCodes []uint `yaml:"httpSuccessCodes" json:"http_success_codes"`
	Host             string `yaml:"host" json:"host"`
	Port             uint16 `yaml:"port" json:"port"`
	Protocol         string `yaml:"protocol" json:"protocol"`
	Period           string `yaml:"period" json:"period"`
	WarningThreshold string `yaml:"warningThreshold" json:"warning_threshold"`
	ErrorThreshold   string `yaml:"errorThreshold" json:"error_threshold"`
	LastStatus       string `yaml:"LastStatus" json:"last_status"`
}

// isValidStatusCode returns whether the given status code might be considered as valid or not
// based on the list defined by the user in the configuration file
func (req Http) isValidStatusCode(statusCode int) bool {
	if len(req.HttpSuccessCodes) == 0 {
		return true
	}
	for _, code := range req.HttpSuccessCodes {
		if int(code) == statusCode {
			return true
		}
	}
	return false
}

// checkTimeoutConsistency ranges over all the services and checks if for each service if
// its timeout is not greater than its period
func (services Services) checkTimeoutConsistency() {
	for groupName, serviceGroup := range services {
		for name, service := range serviceGroup {
			if service.errorThreshold > service.period {
				if conf.useGroups {
					exitWithError(fmt.Errorf("bad configuration for service %q of group %q: the errorThreshold is greater than the period", name, groupName))
				} else {
					exitWithError(fmt.Errorf("bad configuration for service %q: the errorThreshold is greater than the period", name))
				}
			}
		}
	}
}

// makeServicesFrom converts a YamlStruct object to a Services object, and returns an error if the given configuration isn't valid.
// It may also crash directly if the period or the timeout of a service is not valid
func (services *Services) makeServicesFrom(ymlServices YamlStruct) error {
	srvcs := Services{}
	for groupName, serviceGroup := range ymlServices.Services {
		if len(serviceGroup) == 0 {
			return fmt.Errorf("bad configuration for group %q: no services found", groupName)
		}
		srvGrp := ServiceGroup{}
		for serv, props := range serviceGroup {
			service := Service{
				group:            groupName,
				name:             serv,
				period:           getDuration(groupName, props.Period, "field 'period' of service '"+serv+"'"),
				warningThreshold: getDuration(groupName, props.WarningThreshold, "field 'warningThreshold' of service '"+serv+"'"),
				errorThreshold:   getDuration(groupName, props.ErrorThreshold, "field 'errorThreshold' of service '"+serv+"'"),
				confIsValid:      true,
			}
			if service.errorThreshold > service.period {
				return fmt.Errorf("bad configuration for service %q of group %q: the errorThreshold is greater than the period", serv, groupName)
			}
			if len(props.Command.CmdName) > 0 {
				service.command = props.Command
				service.command.active = true
			} else if len(props.Request.Url) > 0 {
				service.httpRequest = props.Request
				log("Service making", "Allowed status codes for service", serv+" of group", groupName+":", service.httpRequest.HttpSuccessCodes)
				service.httpRequest.client.Timeout = service.errorThreshold
				service.httpRequest.active = true
			} else if len(props.CheckPort.Host) > 0 {
				if props.CheckPort.Protocol == "" {
					return fmt.Errorf("bad configuration for service %q of group %q: no protocol provided", serv, groupName)
				}
				if !isProtocolValid(props.CheckPort.Protocol) {
					return fmt.Errorf("bad configuration for service %q of group %q: the protocol %q is not supported", serv, groupName, props.CheckPort.Protocol)
				}
				service.checkPort = props.CheckPort
				service.checkPort.strPort = strconv.Itoa(int(props.CheckPort.Port))
				service.checkPort.active = true
			} else {
				return fmt.Errorf("no action (command/http request/port checking) provided for service %q of group %q", serv, groupName)
			}
			// exitWithError(fmt.Errorf("no action (command/http request/port checking) provided for service %q", serv))
			srvGrp[serv] = &service
		}
		srvcs[groupName] = srvGrp
	}
	*services = srvcs
	return nil
}

// startAll starts all the services contained in the Services object
func (services *Services) startAll(reloadPeriod time.Duration, servicesWaitGroup *sync.WaitGroup) {
	sleepTime := time.Duration(math.Floor(float64(reloadPeriod) / float64(services.getServicesCount()+1)))
	log("Start", "Starting services with an interval of", sleepTime, "...")
	for _, serviceGroup := range *services {
		serviceGroup.startGroup(sleepTime, servicesWaitGroup)
	}
}

// reloadConfig applies the given YamlStruct configuration to the program.
// If the given configuration is not valid, it returns the error and keeps running with the previous configuration
func (services *Services) reloadConfig(yamlObj YamlStruct) error {
	fmt.Println(time.Now().Format("2006-01-02 15:04:05.000"), "Reloading config ...")
	conf.load(yamlObj)
	oldServices := *services
	err := services.makeServicesFrom(yamlObj)
	if err != nil {
		fmt.Println(time.Now().Format("2006-01-02 15:04:05.000"), "Failed to reload config:", err)
		return err
	}
	for _, serviceGroup := range oldServices {
		for _, serv := range serviceGroup {
			serv.confIsValid = false
			serv.execStatus = nil
		}
	}
	return nil
}

// getService returns the requested service and true if it was found; an empty service and false if it was not
func (services Services) getService(group, servName string) (Service, bool) {
	serv, found := services[group][servName]
	if found {
		return *serv, true
	}
	return Service{}, false
}

// getServicesCount returns the total number of services
func (services Services) getServicesCount() int {
	total := 0
	for _, serviceGroup := range services {
		total += len(serviceGroup)
	}
	return total
}

// unsetGauges unregisters the gauge of every service of every group
func (services *Services) unsetGauges() {
	for _, group := range *services {
		for _, service := range group {
			prometheus.DefaultRegisterer.Unregister(service.gauge)
		}
	}
}

// startGroup starts all the services contained in the ServiceGroup object
func (group *ServiceGroup) startGroup(interval time.Duration, servicesWaitGroup *sync.WaitGroup) {
	servicesWaitGroup.Add(len(*group))
	for _, service := range *group {
		log(service.group+"/"+service.name, service.toString())
		service.gauge = promauto.NewGauge(prometheus.GaugeOpts{
			Namespace:   "service_monitor",
			Subsystem:   normalizeNameForPrometheus(service.group),
			Name:        normalizeNameForPrometheus(service.name),
			Help:        service.group + " / " + service.name,
			ConstLabels: map[string]string{"hostname": hostname},
		})
		service.run()
		servicesWaitGroup.Done()
		time.Sleep(interval)
	}
}
