package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gopkg.in/yaml.v2"
)

// config represents the settings part of the yaml configuration
type config struct {
	httpRefreshPeriod time.Duration
	maxDataCount      int
	useGroups         bool
}

// load extracts the configuration properties of the given YamlStruct and stores them in the conf object
func (conf *config) load(yamlObj YamlStruct) {
	conf.httpRefreshPeriod = getDuration("", yamlObj.Config.HttpRefreshPeriod, "field 'httpRefreshPeriod' of the config")
	conf.maxDataCount = int(yamlObj.Config.MaxDataCount)
	conf.useGroups = len(yamlObj.Services) > 1

	if conf.maxDataCount < 1 {
		conf.maxDataCount = 100 // default value
	}
}

// setStringFlag defines a program argument of type string
func setStringFlag(strVar *string, name, defaultValue string, required bool) {
	args := os.Args[1:]
	if len(args) >= 2 {
		for ai := 0; ai < len(args)-1; ai++ {
			if args[ai] == name {
				*strVar = args[ai+1]
				return
			}
		}
	}
	if required {
		fmt.Println("[Error] Missing mandatory parameter:", name)
		os.Exit(1)
	}
	*strVar = defaultValue
}

// setBoolFlag defines a program argument of type bool
func setBoolFlag(boolVar *bool, name string, defaultValue bool, required bool) {
	for _, arg := range os.Args[1:] {
		if arg == name {
			*boolVar = true
			return
		}
	}
	if required {
		fmt.Println("[Error] Missing mandatory parameter:", name)
		os.Exit(1)
	}
	*boolVar = defaultValue
}

// checkPortValidity returns whether the given port is valid or not
func checkPortValidity(port string) {
	match, err := regexp.MatchString(`^[0-9]{1,5}$`, port)
	if err != nil {
		exitWithError(fmt.Errorf("given port %q failed to match check regexp: %v", port, err))
	}
	val, err := strconv.Atoi(port)
	if !match || err != nil || val < 0 || val > 65535 {
		exitWithError(fmt.Errorf("given port %q is not valid", port))
	}
	if val < 1024 {
		fmt.Println("The chosen port", val, "is a privileged one; you must be root to start a web server on it.")
	}
}

// isProtocolValid retrns whether the given network (protocol) is supported or not.
// For further information please check https://pkg.go.dev/net#Dial
func isProtocolValid(proto string) bool {
	match, err := regexp.MatchString(`^ip[46]:(.+)$`, proto) // Regex check for eg "ip4:1" or "ip6:ipv6-icmp"
	if err != nil {
		exitWithError(fmt.Errorf("given protocol %q failed to match regexp: %v", proto, err))
	}
	if match {
		return true
	}
	for _, ntrwk := range []string{"tcp", "tcp4", "tcp6", "udp", "udp4", "udp6", "ip", "unix", "unixgram", "unixpacket"} {
		if proto == ntrwk {
			return true
		}
	}
	return false
}

// log prints a message on the standard output prefixed by the current date and the given theme.
// It will work only if the debug mode is enabled
func log(theme string, a ...interface{}) {
	if debugEnabled {
		fmt.Print(time.Now().Format("2006-01-02 15:04:05.000") + " [" + theme + "] ")
		fmt.Println(a...)
	}
}

// exitWithError prints the given error prefixed with the current date, and exits with the error code 1
func exitWithError(err error) {
	fmt.Println("\n/!\\ Fatal Error /!\\")
	fmt.Println(time.Now().Format("2006-01-02 15:04:05"))
	fmt.Println("- - - - - - - - - - -")
	fmt.Println(err)
	os.Exit(1)
}

// loadYaml parses the file at the given path and stores the result to a new YamlStruct object
func loadYaml(confPath string) YamlStruct {
	// confBytes, err := ioutil.ReadFile(confPath) // Switch to os.ReadFile in Go 1.16 (ioutil is deprecated)
	confBytes, err := os.ReadFile(confPath)
	if err != nil {
		exitWithError(fmt.Errorf("unable to open config file: %v", err))
	}
	var obj YamlStruct
	err = yaml.Unmarshal(confBytes, &obj)
	if err != nil {
		exitWithError(err)
	}
	return obj
}

// encodeAndWrite converts the given data to the given format and writes it on the given output
func encodeAndWrite(w http.ResponseWriter, format string, data interface{}) {
	if len(format) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "No format provided !")
		return
	}
	var err error
	switch strings.ToLower(format) {
	case "json":
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(data)
	case "yml", "yaml":
		w.Header().Set("Content-Type", "application/x-yaml")
		w.WriteHeader(http.StatusOK)
		err = yaml.NewEncoder(w).Encode(data)
	case "xml":
		w.Header().Set("Content-Type", "application/xml")
		w.WriteHeader(http.StatusOK)
		err = xml.NewEncoder(w).Encode(data)
	default:
		w.WriteHeader(http.StatusBadRequest)
		_, err = fmt.Fprintf(w, "Unknown format %q", format)
	}
	if err != nil {
		log("export encoding", err)
	}
}

// displayHelp prints a help message on the standard output
func displayHelp(volontary bool) {
	help := "[" + version + "] " + `Service Monitoring Program 

OPTIONS
	--conf <conf path>
		Specifies the path to the configuration file

	--port <server port>
		Specifies the port on which the HTTP server listens to
		Default: 8080

	--debug
		Indicates whether the program should display debug information
		Default: no debug
	
	--help
		This help

CONFIGURATION FILE EXAMPLE
services:
  group_1:                            # The name of the service group
    filesystem1:                      # The service name
      shell:                          # The service type
        command: "/bin/ls"            # Shell command
        args: [ "/tmp/filesystem1" ]  # Shell command arguments
      period: 5s                      # The service period
      warningThreshold: 1s            # Warning threshold
      errorThreshold: 3s              # Error threshold
    "Google site":
      http:
        url: "https://google.fr"      # The host's address
      period: 6s
      warningThreshold: 2s
      errorThreshold: 4s
    dns:
      checkPort:
        host: "127.0.0.1"             # The host's address
        port: 53                      # The port to check
      period: 6s
      warningThreshold: 2s
      errorThreshold: 4s
  group_2:
	ssh:
      checkPort:
        host: "127.0.0.1"
        port: 22
      period: 6s
      warningThreshold: 2s
      errorThreshold: 5s
config:
  httpRefreshPeriod: 5s               # The interval at which status are checked
  maxDataCount: 75                    # The maximum count of data in history (default value: 100 / minimum: 1)
`
	fmt.Print(help)

	if volontary {
		var port string
		setStringFlag(&port, "--port", "", false)
		if len(port) > 0 {
			checkPortValidity(port)
			fmt.Println("You can read the documentation on http://localhost:" + port)
			doc := Doc{path: "/"}
			doc.SoloStart(port)
		}
		os.Exit(0)
	}
	os.Exit(1)
}
