package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"sync"

	"ServiceMonitor/src/static"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var version = "0.5.0-dev"

var debugEnabled bool
var conf config

// go run main.go <--conf <conf path>> [--port <server port>] [--debug]
// server port default: 8080
// debug default: false
func main() {

	if runtime.GOOS != "linux" {
		exitWithError(errors.New("this program can only be run on a linux system"))
	}

	if len(os.Args) < 2 {
		displayHelp(false)
	}

	var dispHelp bool
	setBoolFlag(&dispHelp, "--help", false, false)
	if dispHelp {
		displayHelp(true)
	}

	var confPath string
	var serverPort string
	var yamlObj YamlStruct
	var services = Services{}
	/*var servSuccess sync.Map
	var timeSuccess sync.Map*/
	// var refreshInterval time.Duration

	setStringFlag(&confPath, "--conf", "", true)
	setStringFlag(&serverPort, "--port", "8080", false)
	setBoolFlag(&debugEnabled, "--debug", false, false)

	checkPortValidity(serverPort)

	fmt.Println("\nStarting ServiceMonitor version", version)

	log("Config", "File path:", confPath)
	log("Config", "Server port:", serverPort)
	log("Config", "Debug:", debugEnabled, "^^")

	yamlObj = loadYaml(confPath)

	conf.load(yamlObj)
	// refreshInterval = getDuration(yamlObj.Config.HttpRefreshPeriod, "field 'httpRefreshPeriod' of the config")
	log("Config", "Http refresh interval:", conf.httpRefreshPeriod)

	err := services.makeServicesFrom(yamlObj)
	if err != nil {
		exitWithError(err)
	}
	if len(services) < 1 || services.getServicesCount() < 1 {
		fmt.Println("No services found, exiting")
		os.Exit(0)
	}

	services.checkTimeoutConsistency()
	var servicesWaitGroup sync.WaitGroup
	services.startAll(conf.httpRefreshPeriod, &servicesWaitGroup)

	/*go func() {
		servicesWaitGroup.Wait()
	}()*/

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			notFoundHandler(w, r)
			return
		}
		historyHandler(services.makeHistoryMapFrom(), strconv.FormatFloat(conf.httpRefreshPeriod.Seconds(), 'g', -1, 32), w, r)
	})
	http.HandleFunc("/export", func(w http.ResponseWriter, r *http.Request) {
		exportHandler(services.makeStatusMapFrom(), w, r)
	})
	http.HandleFunc("/export/history", func(w http.ResponseWriter, r *http.Request) {
		exportHistoryHandler(services.makeHistoryMapFrom(), conf.httpRefreshPeriod.Seconds(), w, r)
	})
	http.HandleFunc("/export/properties", func(w http.ResponseWriter, r *http.Request) {
		exportPropertiesHandler(&services, w, r)
	})
	http.HandleFunc("/reload", func(w http.ResponseWriter, r *http.Request) {
		reloadHandler(confPath, &services, w, r)
	})

	http.Handle("/metrics", promhttp.Handler())

	http.HandleFunc("/static/", staticHandler)

	// Documentation part
	doc := DocFrom("/doc/")
	http.Handle("/doc/", doc)
	static.InitDocPath(doc.path)

	http.HandleFunc("/favicon.ico", static.PrintFavicon)

	fmt.Println(getStartMessage(&services, serverPort))
	fmt.Println("You can see the status history on http://localhost:" + serverPort)

	err = http.ListenAndServe(":"+serverPort, nil)
	if err != nil {
		exitWithError(err)
	}
}

// getStartMessage returns a formatted strings containing the number of services and groups and the port of the http server
func getStartMessage(services *Services, serverPort string) string {
	servNbr, groupNbr := services.getServicesCount(), len(*services)
	servS, groupS := "", ""
	if servNbr > 1 {
		servS = "s"
	}
	if groupNbr > 1 {
		groupS = fmt.Sprintf(" over %d groups", groupNbr)
	}
	return fmt.Sprintf("\n%d service%s loaded%s, starting http server on port %s.\n", servNbr, servS, groupS, serverPort)
}
