package static

import (
	_ "embed"
	"html/template"
	"net/http"
)

//go:embed resources/doc/index.tmpl
var docIndex string

//go:embed resources/doc/server.tmpl
var docServer string

//go:embed resources/doc/config.tmpl
var docConfig string

//go:embed resources/doc/ui.tmpl
var docUI string

var docPath string

func InitDocPath(path string) {
	docPath = path
}

func PrintDocIndex(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	template.Must(template.New("DocIndex").Parse(docIndex)).Execute(w, struct {
		DocPath string
	}{DocPath: docPath})
}

func PrintDocServer(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	template.Must(template.New("DocServer").Parse(docServer)).Execute(w, struct {
		DocPath string
	}{DocPath: docPath})
}

func PrintDocConfig(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	template.Must(template.New("DocConfig").Parse(docConfig)).Execute(w, struct {
		DocPath string
	}{DocPath: docPath})
}

func PrintDocUI(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	template.Must(template.New("DocUI").Parse(docUI)).Execute(w, struct {
		DocPath string
	}{DocPath: docPath})
}
