package static

import (
	_ "embed"
	"html/template"
)

//go:embed resources/404.html
var notFoundTemplate string

//go:embed resources/history.tmpl
var historyTemplate string

type StatusFill struct {
	SuccessMap template.JS
	Hostname   string
	Version    string
}

type HistoryFill struct {
	HistoryMap template.JS
	Interval   template.JS
	Hostname   string
	Version    string
}

func Get404Template() (*template.Template, error) {
	return template.New("status").Parse(notFoundTemplate)
}

func GetHistoryTemplate() (*template.Template, error) {
	return template.New("history").Parse(historyTemplate)
}
