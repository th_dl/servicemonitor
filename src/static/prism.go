package static

import (
	_ "embed"
	"fmt"
	"net/http"
)

//go:embed resources/prism.css
var prismCss string

//go:embed resources/prism.js
var prismJs string

func PrintPrismCss(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/css; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, prismCss)
}

func PrintPrismJs(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/javascript; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, prismJs)
}
