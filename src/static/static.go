package static

import (
	_ "embed"
	"fmt"
	"net/http"
)

//go:embed resources/global.css
var globalCss string

//go:embed resources/doc/doc.css
var docCss string

//go:embed resources/DejaVuSansMono.ttf
var dejaVuSansMono []byte

//go:embed resources/img/icon_2.png
var faviconData []byte

func PrintGlobalCss(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/css; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, globalCss)
}

func PrintDocCss(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/css; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, docCss)
}

func PrintDejaVuSansMono(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "font/ttf")
	w.WriteHeader(http.StatusOK)
	w.Write(dejaVuSansMono)
}

func PrintFavicon(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "image/png")
	w.WriteHeader(http.StatusOK)
	w.Write(faviconData)
}
