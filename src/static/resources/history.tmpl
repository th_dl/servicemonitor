<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Service Monitor [{{.Hostname}}] - Interval of {{.Interval}}s</title>
    <link rel="stylesheet" href="/static/global.css">
</head>
<body>
<div class="header">
    <div>
        <h1>Service Monitor</h1>
        <h6 id="version">{{.Version}}</h6>
    </div>
    <div class="search-div" title="Search for some services">
        <input id="service-search" name="service-search" type="search" placeholder="">
        <label id="service-search-label" for="service-search">Search for services</label>
    </div>
    <div class="options">
        <div>
            <span>Light</span>
            <label for="theme-slider" class="switch">
                <input id="theme-slider" type="checkbox" tabindex="1" accesskey="t">
                <span class="slider"></span>
            </label>
            <span>Dark</span>
        </div>
        <i class="vert-spacer"></i>
        <div id="interval-div" style="padding-top: 2px" title="Server refresh interval: {{.Interval}}s">
            <label for="interval-input">Refresh:</label>
            <input id="interval-input" type="number" min="5" max="3600" step="5" tabindex="2" accesskey="r">
        </div>
        <i class="vert-spacer"></i>
        <div>
            <button id="reload-config" class="options-button"
                    title="Click to reload the configuration of the server" tabindex="3" accesskey="c">Reload config
            </button>
        </div>
    </div>
</div>
<div class="main-row" id="main-div"></div>
<h1 id="danger-text">No data is available ... yet ;-)</h1>
<h1 id="mbm" accesskey="m">Made by Maxi</h1>
<div class="modal-bg">
    <div id="props-modal" class="modal-content">
        <span class="modal-close">&times;</span>
        <h2 id="modal-title"></h2>
        <!--<div id="modal-loader" style="display: none"></div>-->
        <pre id="modal-text"></pre>
    </div>
    <div id="error-modal" class="modal-content"></div>
</div>
<a id="doc-link" href="/doc" target="_blank" title="Click to open the documentation in a new tab">Check documentation
    here</a>

<script>
    let interval = {{.Interval}};
    let jsonHistory = {{.HistoryMap}};
    let useGroups = objectSize(jsonHistory) > 1;
    const statusWidth = 65;
    const maxCellContentSize = "Timeout".length;
    // let itemsDisplayed = 0;
    let reqs = 0;
    let online = true;
    let resizeAlreadyTriggered = false;

    function createTd(servStatus, td = null) {
        td == null ? td = document.createElement("td") : null;
        if (servStatus == null || servStatus.status == null) {
            td.innerText = "?";
            td.classList.add("unknown");
        } else {
            td.setAttribute("execTime", servStatus["exec_time"]);
            if (servStatus.status === "OK") {
                td.innerHTML = servStatus["exec_duration"];
                td.classList.add("on");
                td.title = servStatus["exec_duration"] + " ms";
            } else if (servStatus.status === "WARN") {
                td.innerHTML = servStatus["exec_duration"];
                td.classList.add("warn");
                td.title = servStatus["exec_duration"] + " ms";
            } else if (servStatus.status === "TIMEOUT") {
                td.innerHTML = "Timeout";
                td.classList.add("off");
            } else {
                td.innerHTML = "Error";
                td.classList.add("off");
            }
        }
        let before = true;
        while (maxCellContentSize - td.innerHTML.replaceAll("&nbsp;", "_").length > 0) {
            if (before) {
                td.innerHTML = "&nbsp;" + td.innerHTML;
                before = false;
            } else {
                td.innerHTML += "&nbsp;";
                before = true;
            }
        }
        return td;
    }

    function extractExecTime(td, ref = null) {
        let date;
        if (td.hasAttribute("exectime")) {
            date = new Date(Number(td.getAttribute("exectime")) * 1000);
        } else {
            date = new Date(ref != null ? ref : Date.now());
        }
        const hours = date.getHours().toString();
        const mins = date.getMinutes().toString();
        const secs = date.getSeconds().toString();
        return (hours.length === 1 ? "0" + hours : hours) + ":" + (mins.length === 1 ? "0" + mins : mins) + ":" + (secs.length === 1 ? "0" + secs : secs);
    }

    function updateDate(ref = null) {
        document.querySelectorAll(".time-from").forEach(e => {
            const id = e.getAttribute("group") + "/" + e.getAttribute("service");
            const tr = document.getElementById(id).querySelector("table.status-table tr");
            e.innerText = extractExecTime(tr.lastElementChild, ref || new Date());
        });
        document.querySelectorAll(".time-to").forEach(e => {
            const id = e.getAttribute("group") + "/" + e.getAttribute("service");
            const tr = document.getElementById(id).querySelector("table.status-table tr");
            const interval = Number(document.getElementById("interval-input").value) || 5;
            e.innerText = extractExecTime(tr.firstElementChild, new Date() - tr.children.length * interval * 1000);
        });
    }

    function adaptCols() {
        let mainDiv = document.getElementById("main-div");
        if (useGroups) {
            while (mainDiv.firstElementChild) {
                mainDiv.removeChild(mainDiv.firstElementChild);
            }
            for (const group in jsonHistory) {
                if (!jsonHistory.hasOwnProperty(group)) {
                    continue;
                }
                const col = document.createElement("div");
                col.id = group;
                col.classList.add("column", "group");
                mainDiv.appendChild(col);
            }
        } else {
            while (mainDiv.querySelector("div.column.group") != null) {
                mainDiv.removeChild(mainDiv.querySelector("div.column.group"));
            }
            if (mainDiv.children.length < 1) {
                const col = document.createElement("div");
                col.classList.add("column");
                mainDiv.appendChild(col);
            }

            while (doesOverflow() && mainDiv.firstElementChild.children.length > 1) {
                let col = document.createElement("div");
                col.classList.add("column");
                mainDiv.appendChild(col);
                generateHistoryFromJson(true);
            }
        }
    }

    function regularizeArraysSize() {
        for (let service in jsonHistory) {
            let arr = jsonHistory[service];
            if (arr.length > 100) {
                jsonHistory[service] = arr.slice(arr.length - 100);
            }
        }
    }

    function updateHistory(resHistory) {
        if (objectSize(resHistory) === 0) {
            addUnknownState();
            return;
        }
        let updated = {};
        for (const group in resHistory) {
            if (!jsonHistory.hasOwnProperty(group)) {
                jsonHistory[group] = {};
            }
            updated[group] = [];
            for (const service in resHistory[group]) {
                const servHistory = resHistory[group][service];
                if (jsonHistory[group].hasOwnProperty(service) && jsonHistory[group][service] != null && servHistory != null) {
                    let newStatus = servHistory[servHistory.length - 1];
                    jsonHistory[group][service].push(newStatus);
                } else {
                    jsonHistory[group][service] = servHistory;
                }
                updated[group].push(service);
            }
        }
        for (const oldGroup in jsonHistory) {
            for (const oldService in jsonHistory[oldGroup]) {
                if (!updated.hasOwnProperty(oldGroup) || !updated[oldGroup].includes(oldService)) {
                    delete jsonHistory[oldGroup][oldService];
                }
            }
        }
    }

    function addUnknownState() {
        if (reqs < 1) {
            document.getElementById("danger-text").style.display = "flex";
            return;
        }
        let all_exists = true;
        for (const group in jsonHistory) {
            for (const service in jsonHistory[group]) {
                if (jsonHistory[group][service] == null) {
                    continue;
                }
                if (jsonHistory[group][service].length >= 100) {
                    jsonHistory[group][service].shift();
                }
                jsonHistory[group][service].push(null);
                if (document.getElementById(group + "/" + service) == null) {
                    all_exists = false;
                }
            }
        }
        if (all_exists && objectSize(jsonHistory) > 0) {
            for (const group in jsonHistory) {
                for (const service in jsonHistory[group]) {
                    let tr = document.getElementById(group + "/" + service).lastElementChild.firstElementChild.firstElementChild;
                    tr.appendChild(createTd(null));
                    while (tr.hasChildNodes() && tr.children.length * statusWidth > (window.innerWidth / document.getElementById("main-div").children.length)
                    || tr.offsetWidth >= window.innerWidth) {
                        tr.removeChild(tr.firstElementChild);
                    }
                }
            }
        }
    }

    function getJsonHistory(reset = true) {
        let timeRef = null;
        const url = location.protocol + "//" + location.host + "/export/history?format=json&dataCount=" + (objectSize(jsonHistory) === 0 ? 100 : 1);
        return new Promise((success, reject) => {
            let request = new XMLHttpRequest();
            request.onload = function () {
                if (this.status === 200 && this.responseText != null) {
                    let jsonResult = JSON.parse(request.responseText);
                    success(jsonResult);
                } else {
                    reject(this.error);
                }
            };
            request.onerror = function (ev) {
                reject(ev);
            }
            request.open("GET", url, false);
            request.send();
        }).then(result => {
            online = true;
            // interval = result.interval;
            document.getElementById("interval-div").title = "Server refresh interval: " + result.interval + "s";
            timeRef = result.last_update;
            useGroups = objectSize(result.history) > 1;
            updateHistory(result.history);
            generateHistoryFromJson(reset);
        }).catch(reason => {
            online = false;
            console.warn("XMLHttpRequest did not succeed:", reason);
            addUnknownState();
        }).finally(() => {
            reqs++;
            updateDate(timeRef);
            setTimeout(getJsonHistory, interval * 1000); // Refresh interval
            if (reqs % 10 === 0) {
                regularizeArraysSize();
            }
        });
    }

    function openModal(group, service, html = null) {
        let bgModal = document.querySelector(".modal-bg");
        if (bgModal == null) {
            console.warn("Modal background isn't defined !");
            return;
        }
        let propsModal = bgModal.querySelector("#props-modal");
        let errorModal = bgModal.querySelector("#error-modal");
        if (propsModal == null || errorModal == null) {
            console.warn("All modals are not defined !");
            return;
        }
        // if (bgModal.style.display === "block") {
        propsModal.style.display = "none";
        errorModal.style.display = "none";
        bgModal.style.display = "none";
        // }
        if (html != null) {
            errorModal.innerHTML = "<span class='modal-close'>&times;</span><h2 style='color: #ff0033'>" + service + "</h2>";
            errorModal.innerHTML += html;
            errorModal.querySelector(".modal-close").addEventListener("click", function () {
                document.querySelector(".modal-bg").style.display = "none";
            });
            errorModal.style.display = "block";
            bgModal.style.display = "block";
            return;
        }
        propsModal.querySelector("#modal-title").innerText = service;
        propsModal.style.display = "block";
        bgModal.style.display = "block";
        bgModal.querySelector("#modal-text").innerHTML = "<div id=\"modal-loader\"></div>";
        let url = location.protocol + "//" + location.host + "/export/properties?group=" + encodeURIComponent(group) + "&service=" + encodeURIComponent(service) + "&format=json";
        return new Promise((success, reject) => {
            let request = new XMLHttpRequest();
            request.onload = function () {
                if (this.status === 200 && this.responseText != null) {
                    let jsonResult = JSON.parse(request.responseText);
                    success(jsonResult);
                } else {
                    reject(this.error);
                }
            };
            request.onerror = function (ev) {
                reject(ev);
            }
            request.open("GET", url, false);
            request.send();
        }).then(result => {
            let pre = bgModal.querySelector("#modal-text");
            pre.innerHTML = "";
            if (result.group.length > 0) {
                pre.innerText = "Group: " + result.group + "\n";
            }
            switch (result.type) {
                case "command":
                    pre.innerText += "Service type: Bash command\n";
                    pre.innerText += "\nCommand: " + result.command;
                    pre.innerText += "\nArguments: " + result.arguments;
                    break;
                case "httpRequest":
                    pre.innerText += "Service type: Http request\n";
                    pre.innerText += "\nUrl: " + result.url;
                    pre.innerText += "\nHttp Success Codes:";
                    if (result["http_success_codes"] == null || result["http_success_codes"].length < 1) {
                        pre.innerText += " [ all ]";
                    } else {
                        pre.innerText += " [" + result["http_success_codes"].join(", ") + "]";
                    }
                    break;
                case "checkPort":
                    pre.innerText += "Service type: Port checking\n";
                    pre.innerText += "\nHost: " + result.host;
                    pre.innerText += "\nPort: " + result.port;
                    pre.innerText += "\nProtocol: " + result.protocol;
                    break;
                default:
                    console.warn("Unknown service type:", result.type);
                    return;
            }
            pre.innerText += "\nPeriod: " + result["period"];
            pre.innerText += "\nWarning threshold: " + result["warning_threshold"];
            pre.innerText += "\nError threshold: " + result["error_threshold"];
            pre.innerText += "\nLast status: " + result["last_status"];
            // pre.style.visibility = "visible";
        }).catch(reason => {
            bgModal.querySelector("#modal-text").innerText = reason.toString();
        });
    }

    async function reloadConfig() {
        console.log("Reloading server config ...");
        let url = location.protocol + "//" + location.host + "/reload";
        return new Promise((success, reject) => {
            let request = new XMLHttpRequest();
            request.onload = function () {
                if (this.status === 200 && this.responseText != null) {
                    success(this.response);
                } else {
                    console.info(this);
                    if (this.status === 200) {
                        reject("Server's response is empty")
                    }
                    if (this.response == null || this.response.length < 1) {
                        reject(this.status + " - " + this.error);
                    } else {
                        reject([this.status, this.response]);
                    }
                }
            };
            request.onerror = function (ev) {
                reject(ev);
            }
            request.open("POST", url, false);
            request.send();
        }).then(response => {
            console.info("Success:", response);
            location.reload();
        }).catch(reason => {
            console.warn("Failed to reload config:", reason);
            if (reason.length === 2) {
                openModal(null, "Error while reloading config:", "<h3>" + reason[0] + "</h3><h4>" + reason[1] + "</h4>");
            } else {
                openModal(null, "Error while reloading config:", "<h3>" + reason + "</h3>");
            }
        });
    }

    function objectSize(obj) {
        if (obj instanceof Object) {
            return Object.keys(obj).length;
        }
        return -1;
    }

    function generateHistoryFromJson(reset, updateTime = false) {
        if (objectSize(jsonHistory) < 1) {
            document.getElementById("danger-text").style.display = "flex";
        } else {
            document.getElementById("danger-text").style.display = "";
        }
        let mainDiv = document.getElementById("main-div");
        let hiddens = [];
        if (reset) { // TODO
            for (let col of mainDiv.children) {
                while (col.hasChildNodes()) {
                    if (col.firstElementChild.classList.contains("hidden")) {
                        let srv = col.firstElementChild.getAttribute("service");
                        srv != null ? hiddens.push(srv) : null;
                    }
                    col.removeChild(col.firstChild);
                }
            }
        }
        if (useGroups) {
            adaptCols();
        }
        let colIndex = 0;
        for (const group in jsonHistory) {
            if (!jsonHistory.hasOwnProperty(group)) {
                continue;
            }
            for (const service in jsonHistory[group]) {
                if (!jsonHistory[group].hasOwnProperty(service) || jsonHistory[group][service] == null) {
                    continue;
                }
                const serviceHistory = jsonHistory[group][service];
                let div = document.createElement("div");
                div.id = group + "/" + service;
                div.classList.add("master");
                if (hiddens.includes(service)) {
                    div.classList.add("hidden");
                }
                div.setAttribute("group", group);
                div.setAttribute("service", service);
                let title = document.createElement("h3");
                title.innerText = service;
                title.title = "Click to get more details about service '" + service + "'";
                title.addEventListener("click", function () {
                    openModal(group, service);
                });
                div.appendChild(title);
                let timeTable = document.createElement("table");
                timeTable.classList.add("time-table");
                let timeTr = timeTable.insertRow();
                let toTd = timeTr.insertCell(0);
                // toTd.id = "time-to-" + service;
                toTd.setAttribute("service", service);
                toTd.setAttribute("group", group);
                toTd.classList.add("time-to");
                toTd.innerText = "...";
                let fromTd = timeTr.insertCell(1);
                fromTd.setAttribute("service", service);
                fromTd.setAttribute("group", group);
                fromTd.classList.add("time-from");
                fromTd.innerText = "...";
                div.appendChild(timeTable);
                let table = document.createElement("table");
                table.classList.add("status-table");
                let tbody = document.createElement("tbody");
                let tr = document.createElement("tr");
                tr.id = service + "-tr-status";
                let statusList = [...serviceHistory];
                statusList.reverse().every(servStatus => {
                    let td = tr.insertCell(0);
                    createTd(servStatus, td);
                    return tr.children.length * statusWidth < (window.innerWidth / mainDiv.children.length);
                });
                tbody.appendChild(tr);
                table.appendChild(tbody);
                div.appendChild(table);
                if (useGroups) {
                    document.getElementById(group).appendChild(div);
                } else {
                    mainDiv.children[colIndex].appendChild(div);
                    colIndex++;
                    if (colIndex >= mainDiv.children.length) {
                        colIndex = 0;
                    }
                }
            }
            if (useGroups) {
                colIndex++;
            }
        }
        if (!useGroups && doesOverflow()) {
            adaptCols();
        } else if (updateTime) {
            updateDate();
        }
        // itemsDisplayed = objectSize(jsonHistory) > 0 ? document.querySelector("table.status-table tr").children.length : 0;
    }

    document.addEventListener("DOMContentLoaded", () => {
        adaptCols();
        if (objectSize(jsonHistory) < 1) {
            getJsonHistory(false)
        } else {
            generateHistoryFromJson(false, true);
            setTimeout(getJsonHistory, interval * 1000);
        }
        document.getElementById("reload-config").addEventListener("click", reloadConfig);
        let intrvlInpt = document.getElementById("interval-input");
        intrvlInpt.value = interval;
        intrvlInpt.addEventListener("change", function () {
            if (intrvlInpt.value > 0 && intrvlInpt.value <= 3600) {
                interval = Math.round(intrvlInpt.value);
                intrvlInpt.value = interval;
            }
        });
        let mbm = document.getElementById("mbm");
        mbm.addEventListener("click", function () {
            // mbm.style.display = "block";
            mbm.style.fontSize = "50px";
            setTimeout(() => {
                // mbm.style.display = "";
                mbm.style.fontSize = "0";
            }, 3000);
        });
        if (document.getElementById("theme-slider").checked) {
            document.body.classList.add("dark-theme");
        }
        document.getElementById("theme-slider").addEventListener("click", function (ev) {
            if (ev.target.checked) {
                document.body.classList.add("dark-theme");
            } else {
                document.body.classList.remove("dark-theme");
            }
        });
        document.querySelectorAll(".modal-content").forEach(e => {
            e.addEventListener("click", function (ev) {
                ev.stopImmediatePropagation();
            });
        });
        document.querySelectorAll(".modal-close").forEach(e => {
            e.addEventListener("click", function () {
                document.querySelector(".modal-bg").style.display = "none";
            });
        });
        document.querySelector(".modal-bg").addEventListener("click", function () {
            document.querySelector(".modal-bg").style.display = "none";
        });
        window.addEventListener("resize", () => {
            if (!resizeAlreadyTriggered) {
                resizeAlreadyTriggered = true;
                setTimeout(windowResize, 100);
            }
        });
        let serviceSearch = document.getElementById("service-search");
        serviceSearch.value = "";
        serviceSearch.addEventListener("input", function () {
            let regexp = new RegExp(serviceSearch.value, "gi");
            document.querySelectorAll("div.master").forEach(div => {
                let service = div.getAttribute("service");
                if (service.match(regexp)) {
                    div.classList.remove("hidden");
                } else {
                    div.classList.add("hidden");
                }
            });
        });
    });

    function doesOverflow() {
        return document.body.offsetHeight > window.innerHeight;
    }

    function getHeight(element) {
        return element.offsetHeight || element.innerHeight;
    }

    function windowResize() {
        resizeAlreadyTriggered = false;
        if (document.querySelectorAll("table.status-table").length > 0 && window.innerHeight > 100) {
            let mainDiv = document.getElementById("main-div");
            if (doesOverflow()) {
                adaptCols();

            } else if (mainDiv.children.length > 1) {
                if (getHeight(mainDiv.firstElementChild) + Math.ceil(mainDiv.lastElementChild.children.length * getHeight(mainDiv.lastElementChild.firstElementChild) / (mainDiv.children.length - 1)) < window.innerHeight) {
                    mainDiv.removeChild(mainDiv.lastChild);
                    generateHistoryFromJson(true, true);

                }
            }
            // if (Math.ceil(window.innerWidth / 1000) !== mainDiv.children.length && mainDiv.children.length > 0) {} // Merci William
            /*for (let col of mainDiv.children) {
                col.querySelectorAll("table.status-table tr").forEach(tr => {
                    let service = tr.parentElement.parentElement.parentElement.id;
                    let history = jsonHistory[service];
                    while (tr.children.length * statusWidth < (window.innerWidth / mainDiv.children.length) && history.length > tr.children.length) {
                        tr.insertBefore(createTd(history[history.length - tr.children.length]), tr.firstChild);
                    }
                    while (tr.children.length * statusWidth > (window.innerWidth / mainDiv.children.length) && tr.hasChildNodes()) {
                        tr.removeChild(tr.firstChild);
                    }
                });
                // itemsDisplayed = col.querySelectorAll("table.status-table tr").length;
            }
            updateDate();*/
        }
    }
</script>
</body>
</html>