<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ServiceMonitor - Documentation</title>
    <link rel="stylesheet" href="/static/doc.css">
    <link rel="stylesheet" href="/static/prism.css">
</head>
<body>
<header>
    <h2>Documentation</h2>
    <h1>Service Monitor</h1>
    <ul class="nav">
        <li class="active">Index</li>
        <li>
            <a href="{{.DocPath}}server">Server</a>
            <!--<a href="{{.DocPath}}/server">Server</a>-->
        </li>
        <li>
            <a href="{{.DocPath}}config">Config</a>
        </li>
        <li>
            <a href="{{.DocPath}}ui">UI</a>
        </li>
    </ul>
</header>
<div class="container">
    <section id="intro">
        <h3>Intro</h3>
        <p>Welcome on <i>Service Monitor</i> 's documentation !</p>
    </section>
    <section id="requirements">
        <h3>Requirements</h3>
        <p>First of all, the server must be run on a linux device. Otherwise, it won't start.</p>
        <p>The mandatory arguments must be provided (see the list of arguments <a
                    href="{{.DocPath}}server#starting-arguments">here</a>).</p>
        <p>The path to the config file must be valid and the server must have read access to it.</p>
        <p>The port on which the server must listen must be vacant.</p>
    </section>
    <section id="prometheus">
        <h3>Prometheus</h3>
        <p>A <a href="https://prometheus.io/">Prometheus</a> export is available under the route <code class="concise">/metrics</code>.
        </p>
        <p>Since Prometheus is strict on metrics names, we recommend you to choose names with only alphanumerics and
            underscore characters for groups and services. If you don't follow these recommendations, other characters
            will be replaced by underscores. Furthermore, uppercase characters will be lowercased.</p>
        <p>The final metric name that will be displayed on the <code class="concise">/metrics</code> route will be
            rendered as: <kbd>service_monitor_&lt;group&gt;_&lt;service&gt;</kbd></p>
        <p>The value <kbd>-1</kbd> is assigned to failed services.</p>
    </section>
</div>
<script src="/static/prism.js"></script>
</body>
</html>