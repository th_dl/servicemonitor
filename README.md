# Service Monitor [V0.5.0]

### A Golang-written service monitoring program with a web interface

### **Go version: 1.19**

## Compilation (statically linked):

```shell
(cd src && go build -ldflags="-extldflags=-static" -tags osusergo,netgo -o service_monitor)
```

## Config example:

```yaml
services:
  group_1: # The name of the service group
    filesystem1: # The service name
      shell: # The service type
        command: "/bin/ls"            # Shell command
        args: [ "/tmp/filesystem1" ]  # Shell command arguments
      period: 5s                      # The service period
      warningThreshold: 1s            # Warning threshold
      errorThreshold: 3s              # Error threshold
    "Google site":
      http:
        url: "https://google.fr"      # The host's address
      period: 6s
      warningThreshold: 2s
      errorThreshold: 4s
    dns:
      checkPort:
        host: "127.0.0.1"             # The host's address
        port: 53                      # The port to check
      period: 6s
      warningThreshold: 2s
      errorThreshold: 4s
  group_2:
    ssh:
      checkPort:
        host: "127.0.0.1"
        port: 22
      period: 6s
      warningThreshold: 2s
      errorThreshold: 5s
config:
  httpRefreshPeriod: 5s               # The interval at which status are checked
  maxDataCount: 75                    # The maximum count of data in history (default value: 100 / minimum: 1)
```
